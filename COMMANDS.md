# Commands

## GOKZ Core

### Timer

 * ```!checkpoint``` - Set a checkpoint.
 * ```!gocheck``` - Teleport to your current checkpoint.
 * ```!prev``` - Go back a checkpoint.
 * ```!next``` - Go forward a checkpoint.
 * ```!pause```/```!resume``` - Toggle pausing your timer and stopping you in your position.
 * ```!undo``` - Undo teleport.
 * ```!start```/```!r``` - Teleport to the start of the map.
 * ```!stop``` - Stop your timer.
 * ```!setstartpos```/```!ssp``` - Set your current position as your custom start position.
 * ```!clearstartpos```/```!csp``` - Clear your custom start position.

### Options

 * ```!options``` - Open the options menu.
 * ```!menu``` - Toggle the visibility of the simple teleport menu.
 * ```!adv``` - Toggle the visibility of the advanced teleport menu.
 * ```!hide``` - Toggles the visibility of other players.
 * ```!speed``` - Toggle visibility of the centre information panel.
 * ```!hideweapon``` - Toggle visibility of your weapon.
 * ```!pistol``` - Open the pistol selection menu.
 
### Modes

 * ```!mode``` - Opens up the movement mode selection menu.
 * ```!vanilla```/```!v``` - Switch to the Vanilla mode.
 * ```!simplekz```/```!s``` - Switch to the SimpleKZ mode.
 * ```!kztimer```/```!k``` - Switch to the KZTimer mode.

### Other

 * ```!nc``` - Toggle noclip.
 * ```+noclip``` - Noclip (bind a key to it).
 * ```!spec``` - Join spectators or spectate a specified player. Usage ```!spec <player>```
 * ```!goto``` - Teleport to another player. Usage: ```!goto <player>```
 * ```!measure``` - Open the distance measurement menu.
 * ```!stopsound``` - Stop all sounds e.g. map soundscapes (music).
 
## GOKZ Local Ranks

These commands return results based on your currently selected mode.
 
 * ```!top``` - Opens a menu showing the top record holders
 * ```!maptop``` - Opens a menu showing the top times of a map. Usage: ```!maptop <map>```
 * ```!bmaptop``` - Opens a menu showing the top bonus times of a map. Usage: ```!btop <#bonus> <map>```
 * ```!pb``` - Prints map times and ranks to chat. Usage: ```!pb <map> <player>```
 * ```!bpb``` - Prints PB bonus times and ranks to chat. Usage: ```!bpb <#bonus> <map> <player>```
 * ```!wr``` - Prints map record times to chat. Usage: ```!wr <map>```
 * ```!bwr``` - Prints bonus record times to chat. Usage: ```!bwr <#bonus> <map>```
 * ```!avg``` - Prints the average map run time to chat. Usage ```!avg <map>```
 * ```!bavg``` - Prints the average bonus run time to chat. Usage ```!bavg <#bonus> <map>```
 * ```!pc``` - Prints map completion to chat. Usage: ```!pc <player>```
 
## GOKZ Replays

 * ```!replay``` - Opens the replay loading menu.